//
// Created by janmarius on 14.02.19.
//

#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <math.h>
#include <bits/stdc++.h>

using namespace std;

#ifndef OVING1_PRIMES_H
#define OVING1_PRIMES_H


class Primes {

public:
    // Default Constructor
    Primes();

    // Destructor
    ~Primes();

    // Accessor Functions
    int getNumOfThreads() const;
    int getNumOfPrimeNumbers() const;
    vector<int> findAllPrimes(int, int, int);

private:
    mutex primeNumber_mutex;
    mutex numb_Of_Threads;
    vector<int> primeNumbers;
    int numOfPrimeNumbers;
    int numOfThreads;

    bool isPrime(int) const;
    void findPrimes(int, int);
//    void switchPos();


};

#endif //OVING1_PRIMES_H
