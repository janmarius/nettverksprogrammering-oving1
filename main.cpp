#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <math.h>
#include <bits/stdc++.h>
#include "Primes.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace std;


int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    cout << endl;
    cout << "Enter first number: ";
    int firstNumber;
    cin >> firstNumber;
    cout << "Enter second number: ";
    int secondNumber;
    cin >> secondNumber;
    cout << "Enter number of threads: ";
    int numberOfThreads;
    cin >> numberOfThreads;
    cout << endl;

    Primes primes;
    vector<int> primeNumbers = primes.findAllPrimes(firstNumber, secondNumber, numberOfThreads);

    cout << endl;


    cout << "Found " << primes.getNumOfPrimeNumbers() << " primes in the interval " << firstNumber << " -> " << secondNumber << ", with " << primes.getNumOfThreads() << " threads." << "\nSorted primes:" << endl;
    for (int x : primeNumbers) {
        cout << x << " ";
    }
}