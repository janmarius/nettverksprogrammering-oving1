//
// Created by janmarius on 14.02.19.
//

#include "Primes.h"

Primes::Primes() {
    numOfPrimeNumbers = 0;
    numOfThreads = 0;
}

Primes::~Primes() {}

int Primes::getNumOfPrimeNumbers() const {
    return numOfPrimeNumbers;
}

int Primes::getNumOfThreads() const {
    return numOfThreads;
}

vector<int> Primes::findAllPrimes(int firstNumber, int secondNumber, int numberOfThreads) {


    if(firstNumber > secondNumber) {
        int temp = firstNumber;
        firstNumber = secondNumber;
        secondNumber = temp;
    }

    int rangeSize = (secondNumber - (firstNumber - 1));
    int intervalSizeCeiled = ceil((double)rangeSize/(double)numberOfThreads);

    vector<thread> myThreads;

    for(int i = firstNumber; i <= secondNumber; i += intervalSizeCeiled ) {
        int threadSecondNumber = (i + intervalSizeCeiled) - 1;
        if(threadSecondNumber > secondNumber) {
            threadSecondNumber = secondNumber;
        }
        myThreads.emplace_back(thread([this, i, threadSecondNumber] (){
            lock_guard<mutex> lock(numb_Of_Threads);
            numOfThreads++;
            cout << "Thread number " << numOfThreads << ": " << i << " -> " << threadSecondNumber << endl;
            findPrimes(i, threadSecondNumber);
        }));
    }

    auto originalthread = myThreads.begin();

    while (originalthread != myThreads.end()) {
        originalthread->join();
        originalthread++;
    }

    sort(primeNumbers.begin(), primeNumbers.end());

    return primeNumbers;

}


bool Primes::isPrime(int number) const {
    bool isPrime = true;
    if (number <= 1) {
        isPrime = false;
    }
    for (int j = 2; j <= number / 2; ++j) {
        if (number % j == 0) {
            isPrime = false;
        }
    }
    return isPrime;
}

void Primes::findPrimes(int firstNumber, int secondNumber) {
    for(int i = firstNumber; i <= secondNumber; ++i) {
        if(isPrime(i)) {
            lock_guard<mutex> lock(primeNumber_mutex);
            primeNumbers.push_back(i);
            numOfPrimeNumbers++;
        }
    }
}

//void Primes::switchPos() {
//    if(firstNumber > secondNumber) {
//        int temp = firstNumber;
//        firstNumber = secondNumber;
//        secondNumber = temp;
//    }
//}