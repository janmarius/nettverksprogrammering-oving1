//
// Created by janmarius on 14.02.19.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <vector>
#include "Primes.h"

using testing::Eq;

namespace {
    class ClassDeclaration : public testing::Test {
    public:
        Primes obj;
        ClassDeclaration(){
            obj;
        }

    };

}

TEST_F(ClassDeclaration, findAllPrimesTest1) {
    vector<int> vect = {2, 3, 5, 7, 11, 13};
    ASSERT_EQ(obj.findAllPrimes(1, 13, 3), vect);
}

TEST_F(ClassDeclaration, findAllPrimesTest2) {
    vector<int> vect = {2, 3, 5, 7, 11, 13};
    ASSERT_EQ(obj.findAllPrimes(13, 1, 3), vect);
}